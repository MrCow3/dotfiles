# Alex Lorei's bashrc

source /home/alex/.aliases

stty -ixon # disable ctrl-s and ctrl-q
wal -Rq
## Aliases

# vi mode
set -o vi

alias p='sudo pacman'
alias SS='sudo systemctl'
alias vi='vim'
alias mkd='mkdir -pv'
alias c='clear'

# internet aliases
alias yt="youtube-dl --add-metadata -ic"
alias yta="youtube-dl --add-metadata -xic"
# adding color
alias ls='ls --color=auto --group-directories-first'
alias grep='grep --color=auto'
alias highlight='highlight --out-format=ansi'
# PS1
PS1='[\u@\h \W]\$ '

# Starship
eval "$(starship init bash)"
