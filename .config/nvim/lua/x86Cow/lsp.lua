require 'lspconfig'.gopls.setup {
    on_attach = function()
        capabilities = capabilities,
        vim.keymap.set("n", "K", vim.lsp.buf.hover, {buffer = 0})
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, {buffer = 0})
        vim.keymap.set("n", "gT", vim.lsp.buf.type_definition, {buffer = 0})
        vim.keymap.set("n", "<leader>dj", vim.diagnostic.goto_next, {buffer = 0})
        vim.keymap.set("n", "<leader>dj", vim.diagnostic.goto_prev, {buffer = 0})
    end,
}
