-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
-- Packer can manage itself
    use 'wbthomason/packer.nvim'
    use "folke/tokyonight.nvim"

    use "preservim/vimux"
    -- harpoon
    use "nvim-lua/plenary.nvim"
    use "ThePrimeagen/harpoon"

    -- LSP
    use "BurntSushi/ripgrep"
    use "neovim/nvim-lspconfig"
    use {
        'nvim-telescope/telescope.nvim', tag = '0.1.0',
         requires = { {'nvim-lua/plenary.nvim'} }
    }
    -- completion
    use "hrsh7th/nvim-cmp"
    use "hrsh7th/cmp-nvim-lsp"
    use "hrsh7th/cmp-buffer"
    use "hrsh7th/cmp-path"
    use "L3MON4D3/LuaSnip"
    use "saadparwaiz1/cmp_luasnip"

    -- vim-tmux
    use "christoomey/vim-tmux-navigator"

    -- whichkey
    use "folke/which-key.nvim"
  end)
