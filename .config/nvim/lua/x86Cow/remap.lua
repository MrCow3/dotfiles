local nnoremap = require("x86Cow.keymap").nnoremap

-- window movement
nnoremap ("<C-J>", "<C-W><C-J>")
nnoremap ("<C-K>", "<C-W><C-K>")
nnoremap ("<C-L>", "<C-W><C-L>")
nnoremap ("<C-H>", "<C-W><C-H>")

-- window creation
nnoremap ("vv", "<C-W><C-V>")

-- vimux prompt
nnoremap ("<leader>vp", "<cmd>VimuxPromptCommand<CR>")
nnoremap ("<leader>vl", "<cmd>VimuxRunLastCommand<CR>")
nnoremap ("<leader>vi", "<cmd>VimuxInspectRunner<CR>")
nnoremap ("<leader>vz", "<cmd>VimuxZoomRunner<CR>")

-- bind netrw
nnoremap("<leader>pv", "<cmd>Ex<CR>")

-- HARPOON
nnoremap("<leader>hc", "<cmd>:lua require('harpoon.mark').add_file()<CR>")
nnoremap("<leader>hv", "<cmd>:lua require('harpoon.ui').toggle_quick_menu()<CR>")

nnoremap("<leader>ha", "<cmd>:lua require('harpoon.ui').nav_file(1)<CR>")   
nnoremap("<leader>hs", "<cmd>:lua require('harpoon.ui').nav_file(2)<CR>") 
nnoremap("<leader>hd", "<cmd>:lua require('harpoon.ui').nav_file(3)<CR>") 
nnoremap("<leader>hf", "<cmd>:lua require('harpoon.ui').nav_file(4)<CR>")

nnoremap("<leader>hj", "<cmd>:lua require('harpoon.ui').nav_next()<CR>")
nnoremap("<leader>hk", "<cmd>:lua require('harpoon.ui').nav_prev()<CR>")
