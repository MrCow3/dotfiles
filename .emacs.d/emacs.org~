#+Title: My emacs config in org
#+Author: Alex Lorei
#+PROPERTY: header-args:emacs-lisp :tangle ./init.el
* Basic UI config
#+begin_src emacs-lisp
;; make startup message go away for more clean screen
(setq inhibit-startup-message t)

;; theme
(load-theme 'dracula t)

;; visual bell
(setq ring-bell-function
      (lambda ()
        (let ((orig-fg (face-foreground 'mode-line)))
          (set-face-foreground 'mode-line "#F2804F")
          (run-with-idle-timer 0.1 nil
                               (lambda (fg) (set-face-foreground 'mode-line fg))
                               orig-fg))))

;; UI changes (more clean interface)
(column-number-mode)
(global-display-line-numbers-mode t)

(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1) 
(set-fringe-mode 10)
(menu-bar-mode -1)
#+end_src
** Font Configuration
   #+begin_src emacs-lisp
     (set-face-attribute 'default nil :font "firacode" :height 110)
   #+end_src
* Escape Quit All
  #+begin_src emacs-lisp
    ; keybind to make escape key exit prompts
    (global-set-key (kbd "<escape>") 'keyboard-escape-quit)
  #+end_src
* Prepare Use Of Packages
   #+begin_src emacs-lisp
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org"   . "https://orgmode.org/elpa")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)
   #+end_src
* Org Mode
** Auto-tangle
   #+begin_src emacs-lisp
     ;; Automatically tangle our Emacs.org config file when we save it
     (defun efs/org-babel-tangle-config ()
       (when (string-equal (buffer-file-name)
			   (expand-file-name "~/Projects/Code/emacs-from-scratch/Emacs.org"))
	 ;; Dynamic scoping to the rescue
	 (let ((org-confirm-babel-evaluate nil))
	   (org-babel-tangle))))

     (add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'efs/org-babel-tangle-config)))
   #+end_src
   * 
** Babel
   #+begin_src emacs-lisp
     (use-package babel)
     (org-babel-do-load-languages
      'org-babel-load-languages
      '((emacs-lisp . t)
	(python . t)
	(shell . t)))
     (setq org-confirm-babel-evaluate nil)
   #+end_src
** Structure Templates
   #+begin_src emacs-lisp
   (require 'org-tempo)
   (add-to-list 'org-structure-template-alist'("sh" . "src shell"))
   (add-to-list 'org-structure-template-alist'("el" . "src emacs-lisp"))
   #+end_src

** Auto New Line
   #+begin_src emacs-lisp
     (add-hook 'org-mode-hook '(lambda () (setq fill-column 80)))
     (add-hook 'org-mode-hook 'auto-fill-mode)
   #+end_src 
* Evil Mode
  #+begin_src emacs-lisp

    (use-package undo-tree
      :init
      (global-undo-tree-mode 1))
    (use-package evil
      :ensure t
      :init
      (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
      (setq evil-want-keybinding nil)
      :config
      (evil-mode 1))

      (evil-global-set-key 'motion "j" 'evil-next-visual-line)
      (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
    (use-package evil-collection
      :after evil
      :ensure t
      :config
      (evil-collection-init))

   #+end_src
* Ivy Config
  #+begin_src emacs-lisp
    (use-package ivy
      :init (ivy-mode 1))

    (use-package ivy-rich
      :init (ivy-rich-mode 1))
  #+end_src
* Which-key
  #+begin_src emacs-lisp
    (use-package which-key
      :init (which-key-mode)
      :diminish which-key-mode
      :config
      (setq which-key-idle-delay 0.3))
  #+end_src 
* Counsel
  #+begin_src emacs-lisp
    (use-package counsel
     :init (counsel-mode 1))
  #+end_src

* Powerline
  #+begin_src emacs-lisp
    (use-package powerline)
    (powerline-default-theme)
  #+end_src
* EXWM
** preparing exwm
   #+begin_src emacs-lisp


       (defun efs/exwm-update-class ()
	 (exwm-workspace-rename-buffer exwm-class-name)))

       (use-package exwm
	 :config
	 ;; Set the default number of workspaces
	 (setq exwm-workspace-number 5)

	 ;; When window "class" updates, use it to set the buffer name
	 ;; (add-hook 'exwm-update-class-hook #'efs/exwm-update-class)

	 ;; These keys should always pass through to Emacs
	 (setq exwm-input-prefix-keys
	   '(?\C-x
	     ?\C-u
	     ?\C-h
	     ?\M-x
	     ?\M-`
	     ?\M-&
	     ?\M-:
	     ?\C-\M-j  ;; Buffer list
	     ?\C-\ ))  ;; Ctrl+Space

	 ;; Ctrl+Q will enable the next key to be sent directly
	 (define-key exwm-mode-map [?\C-q] 'exwm-input-send-next-key)

	 ;; Set up global key bindings.  These always work, no matter the input state!
	 ;; Keep in mind that changing this list after EXWM initializes has no effect.
	 (setq exwm-input-global-keys
	       `(
		 ;; Reset to line-mode (C-c C-k switches to char-mode via exwm-input-release-keyboard)
		 ([?\s-r] . exwm-reset)

		 ;; Move between windows
		 ([s-left] . windmove-left)
		 ([s-right] . windmove-right)
		 ([s-up] . windmove-up)
		 ([s-down] . windmove-down)

		 ;; Launch applications via shell command
		 ([?\s-&] . (lambda (command)
			      (interactive (list (read-shell-command "$ ")))
			      (start-process-shell-command command nil command)))

		 ;; Switch workspace
		 ([?\s-w] . exwm-workspace-switch)

		 ;; 's-N': Switch to certain workspace with Super (Win) plus a number key (0 - 9)
		 ,@(mapcar (lambda (i)
			     `(,(kbd (format "s-%d" i)) .
			       (lambda ()
				 (interactive)
				 (exwm-workspace-switch-create ,i))))
			   (number-sequence 0 9))))

	 (exwm-enable))


         (use-package xelb)
   #+end_src
