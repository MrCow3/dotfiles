
; init.el
; Alex Lorei's Config

; make startup message go away for more clean screen
(setq inhibit-startup-message t)

; font
(set-face-attribute 'default nil :font "firacode" :height 110)

; theme
(load-theme 'dracula t)


; visual bell
(setq ring-bell-function
      (lambda ()
        (let ((orig-fg (face-foreground 'mode-line)))
          (set-face-foreground 'mode-line "#F2804F")
          (run-with-idle-timer 0.1 nil
                               (lambda (fg) (set-face-foreground 'mode-line fg))
                               orig-fg))))

; UI changes (more clean interface)
(column-number-mode)
(global-display-line-numbers-mode t)

(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1) 
(set-fringe-mode 10)
(menu-bar-mode -1)

; keybind to make escape key exit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; PACKAGES
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org"   . "https://orgmode.org/elpa")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package command-log-mode)

(use-package ivy
  :init (ivy-mode 1))

(use-package ivy-rich
  :init (ivy-rich-mode 1))

(use-package counsel
  :init (counsel-mode 1))

(use-package evil
  :ensure t
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package which-key
  :init (which-key-mode)
  :config
  (setq which-key-idle-dely 0))

(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind(("C-x C-j" . dired-jump))
  :custom ((dired-listing-switches "-agho --group-directories-first"))
  :config
   ; (evil-collection-define-key 'normal 'dired-mode-map
   ;			      "h" dired-up-directory
   ;	      		      "l" dired-find-file)
)

(use-package term
  :config
  (setq explicit-shell-file-name "bash")
  (setq term-prompt-regexp "^#$%>\n]*[#$%>] *"))

; (use-package projectile
;  :config (projectile-mode)
;  :bind-keymap
;  ("C-c p" . projectile-command-map)
;  :init
;  (when (file-directory-p "~/repos/")
;    (setq projectile-project-search-path '("~/repos/")))
;  (setq projectile-switch-project-action #'projectile-dired))

; (use-package magit
;  :ensure t)

; exwm

; (defun efs/exwm-update-class ()
;  (exwm-workspace-rename-buffer exwm-class-name))
; (use-package exwm
;  :config
;  (setq exwm-workspace-number 4)
;  (exwm-enable))
; (setq exwm-input-prefix-keys

(use-package powerline)
(powerline-default-theme)

(use-package org)
;; org babel configuration
(use-package babel)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)
   (shell . t)))
(setq org-confirm-babel-evaluate nil)

(require 'org-tempo)
(add-to-list 'org-structure-template-alist'("sh" . "src shell"))
(add-to-list 'org-structure-template-alist'("el" . "src emacs-lisp"))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("549ccbd11c125a4e671a1e8d3609063a91228e918ffb269e57bd2cd2c0a6f1c6" default))
 '(package-selected-packages
   '(org-bullets babel org-super-agenda powerline evil-collection magit emms projectile dracula-theme which-key use-package rainbow-delimiters ivy-rich evil counsel command-log-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
