(define-package "emms" "20210619.1246" "The Emacs Multimedia System"
  '((cl-lib "0.5")
    (nadvice "0.3")
    (seq "0"))
  :commit "6e0aaaf4c5598826b24c3079b80bf8af000a77c6" :authors
  '(("Jorgen Schäfer" . "forcer@forcix.cx"))
  :maintainer
  '("Yoni Rabkin" . "yrk@gnu.org")
  :keywords
  '("emms" "mp3" "ogg" "flac" "music" "mpeg" "video" "multimedia")
  :url "https://www.gnu.org/software/emms/")
;; Local Variables:
;; no-byte-compile: t
;; End:
