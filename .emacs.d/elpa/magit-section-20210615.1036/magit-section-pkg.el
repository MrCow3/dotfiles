(define-package "magit-section" "20210615.1036" "Sections for read-only buffers"
  '((emacs "25.1")
    (dash "20210330"))
  :commit "c10e10c9a427c192dec841e524a6d3d47d2b21ee" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("tools")
  :url "https://github.com/magit/magit")
;; Local Variables:
;; no-byte-compile: t
;; End:
