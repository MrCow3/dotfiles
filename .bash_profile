#
# ~/.bash_profile
#

## PATH
export PATH=$PATH:/home/alex/.dotfiles/.scripts

# EXPORTS
export BROWSER="firefox"
export EDITOR="vim"
export TERMINAL="st"

[[ -f ~/.bashrc ]] && . ~/.bashrc

